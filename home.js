const express = require('express');
const bodyParser = require('body-parser');
const pdf = require('./pdf'); 

const app = express();
const port = 80;  
 
app.use(express.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: false
  }));

app.post("/pdf", (req, res) => { 
 
    console.log(req.body); 
  
        pdf.pdf(req.body).then(data=> {  
            returnData(res, "sample.pdf", data); 
        }).catch(err => {
            console.log(err);
            res.send("There was an error");
        });
            
    }
);

app.get("/test", (req, res) => res.send("Hello there test"));

app.listen(port, ()=> console.log(`BW listening at port ${port} .....'`)); 


function returnData(res, fileName, data) { 

    res.writeHead(200, {
        'Content-Type': "application/pdf",
        'Content-disposition': 'attachment;filename=' + fileName,
        'Content-Length': data.length
    });

    res.end(data,'binary'); 

}
 