var fs = require('fs'); 

const puppeteer = require('puppeteer');

exports.pdf = (async(model) => { 
 
  const browser = await puppeteer.launch(
     {
       headless:true,
       args: ['--no-sandbox']
     }
  )
  const page = await browser.newPage();
 
  //var fileName = __dirname + "/demo.html";
  //var contentHtml = fs.readFileSync(fileName, 'utf8');
 
  //fileName = "file://" + fileName;≈
   // console.log(fileName);
  //onsole.log(contentHtml);

  //, {waitUntil: 'networkidle0'}
  await page.setContent(model.Body).catch(err=> {
      console.log(err);
      return;
  });

  //https://pptr.dev/#?product=Puppeteer&version=v4.0.0&show=api-pagepdfoptions
  //await page.goto(fileName, {waitUntil: 'networkidle0'});
 
  var pdfOptions = {
    //path: "example" + Date.now() + "_date.pdf",
    format: "letter",
    displayHeaderFooter: true,
    headerTemplate: model.HeaderFooterCss + model.HeaderHtml, 
    footerTemplate: model.FooterHtml
  };
 
  //console.log(pdfOptions);

  var pdf = await page.pdf(pdfOptions)
  .catch((err)=> {
      console.log("Error while generating the PDF", err);
    })

   // console.log(pdf);

  await browser.close(); 

  return pdf;

});


